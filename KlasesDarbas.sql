ALTER TABLE parduotuves 
MODIFY ParduotuvesId INT AUTO_INCREMENT;

INSERT INTO parduotuves (Miestas, Adresas)
VALUES ('Vilnius', 'Testine g. 1');

INSERT INTO parduotuves (Adresas)
VALUES ('Testinė g. 2');

INSERT INTO parduotuves (Miestas, Adresas)
VALUES ('Vilnius', 'Testine g. 3'),
	   ('Kaunas', 'Testine g. 4'),
       ('Panevežys', 'Testine g. 5'),
       ('Kaunas', 'Testine g. 6'),
       ('Radviliškis', 'Testine g. 7')
       
#========================================================]

SELECT [<column_names>, *] FROM <table_name>
SELECT column1, column2 FROM lentele
SELECT * FROM lentele

SELECT * FROM parduotuves
SELECT Adresas, Miestas FROM parduotuves
#======================================================

INSERT INTO parduotuves (Adresas, Miestas)
SELECT Adresas, Miestas FROM temp_parduotuves

SELECT * FROM parduotuves
WHERE Miestas = 'Vilnius'


SELECT * FROM parduotuves
WHERE Adresas LIKE 'Testin_ g. 1_%'
ORDER BY ParduotuvesId DESC
LIMIT 2;

INSERT INTO parduotuves (Miestas, Adresas)
VALUES ('Vilnius', 'Testine g. 26'),
	   ('Vilnius', 'Testinė g. 19'),
       ('Vilnius', 'Testine g. 16C')

SELECT * FROM parduotuves
WHERE ParduotuvesId = 1 OR
	ParduotuvesId = 6 OR 
	ParduotuvesId = 8 OR 

SELECT * FROM Kasos
WHERE ParduotuvesId IN (SELECT ParduotuvesId FROM Parduotuves 
		WHERE Adresas LIKE 'Testin_ g. 1_%')

ALTER TABLE KasosKasininkai
DROP FOREIGN KEY FK_Kasos_KasosId;

ALTER TABLE Kasos 
MODIFY KasosId INT AUTO_INCREMENT;

ALTER TABLE KasosKasininkai
ADD CONSTRAINT FK_Kasos_KasosId
FOREIGN KEY (KasosId) REFERENCES Kasos(KasosId);

       